/*
 * Dummy Account Credentials:
 * 
 * Admin Account:
 * Username: adminAccount 
 * Password: admin
 * 
 * Seller Account:
 * Username: sellerAccount
 * Password: seller
 * 
 * Buyer Account:
 * Username: buyerAccount
 * Password: buyer
 */
package ca.sheridancollege.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ca.sheridancollege.beans.Account;
import ca.sheridancollege.beans.AccountRole;
import ca.sheridancollege.beans.Address;
import ca.sheridancollege.beans.AdminAccount;
import ca.sheridancollege.beans.BuyerAccount;
import ca.sheridancollege.beans.Cart;
import ca.sheridancollege.beans.Product;
import ca.sheridancollege.beans.ProductOptions;
import ca.sheridancollege.beans.SellerAccount;

public class DAO {

	SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

	public Account findUserByUserName(String userName) {

		List<Account> accounts = sessionFactory.openSession().createQuery("from Account where userName=:user")
				.setParameter("user", userName).list();

		if (accounts.size() > 0) {
			return accounts.get(0);
		} else {
			return null;
		}
	}

	public void generateItems() {

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		LocalDate localDate = LocalDate.now();

		ProductOptions options = new ProductOptions("Its a tomato", 50, 4);

		List<Product> productList = new CopyOnWriteArrayList<Product>();
		productList.add(new Product("Tomato", "fruits", options, new SellerAccount()));

		Address address = new Address("Main Street", "Mississauga", "M6J2K2");

		String password1 = "buyer";
		String hashedPassword1 = passwordEncoder.encode(password1);
		String password2 = "seller";
		String hashedPassword2 = passwordEncoder.encode(password2);
		String password3 = passwordEncoder.encode("admin"); // combined

		BuyerAccount buyer = new BuyerAccount("buyerAccount", "buyer@gmail.com", hashedPassword1, localDate, /*
																												 * "buyer",
																												 */
				address, "Donald", "Blake", "active");
		AccountRole ar1 = new AccountRole(buyer, "ROLE_BUYER");
		buyer.getRole().add(ar1);

		SellerAccount seller = new SellerAccount("sellerAccount", "seller@gmail.com", hashedPassword2, localDate, /*
																													 * "seller",																									 */
				address, "Tony", "Stark", "active", "Stark International", 5, productList);
		seller.getProducts().get(0).setSeller(seller);
		AccountRole ar2 = new AccountRole(seller, "ROLE_SELLER");
		seller.getRole().add(ar2);

		AdminAccount admin = new AdminAccount("adminAccount", "admin@gmail.com", password3, localDate, /*																					 * "administrator",
																										 */
				address, "theAdmin");
		AccountRole ar3 = new AccountRole(admin, "ROLE_ADMIN");
		admin.getRole().add(ar3);

		session.save(buyer);
		session.save(seller);
		session.save(admin);
		session.save(ar1);
		session.save(ar2);
		session.save(ar3);

		session.getTransaction().commit();
		session.close();
	}

	public void saveBuyer(BuyerAccount buyer) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		buyer.setPassword(passwordEncoder.encode(buyer.getPassword()));
		AccountRole ar = new AccountRole(buyer, "ROLE_BUYER");
		buyer.getRole().add(ar);
		session.save(buyer);
		session.save(ar);

		session.getTransaction().commit();
		session.close();
	}

	public void saveCart(Cart cart) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		session.save(cart);

		session.getTransaction().commit();
		session.close();
	}

	public List<Account> getAccounts() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Account> criteria = criteriaBuilder.createQuery(Account.class);
		Root<Account> root = criteria.from(Account.class);
		List<Account> voteList = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();

		return voteList;
	}

	public List<Account> getAccountsByQueryUsername(String username) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.createQuery("from Account where userName like :username");
		List<Account> voteList = query.setParameter("username", '%' + username + '%').getResultList();

		session.getTransaction().commit();
		session.close();

		return voteList;
	}

	public Account getAccountByQueryUsername(String username) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.createQuery("from Account where userName like :username");
		List<Account> voteList = query.setParameter("username", '%' + username + '%').getResultList();

		session.getTransaction().commit();
		session.close();

		if (voteList.size() > 1) {
			return null;
		} else {
			return voteList.get(0);
		}
	}

	public void adminDeleteUser(String username) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		session.delete(session.get(Account.class, username));

		session.getTransaction().commit();
		session.close();
	}

	public List<Product> getProducts() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Product> criteria = criteriaBuilder.createQuery(Product.class);
		Root<Product> root = criteria.from(Product.class);
		List<Product> productList = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();

		return productList;
	}

	public List<Product> getProductsById(int productId) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Product> criteria = criteriaBuilder.createQuery(Product.class);
		Root<Product> root = criteria.from(Product.class);
		criteria.where(criteriaBuilder.equal(root.get("productId"), productId));
		List<Product> productList = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();

		return productList;
	}

	public List<Product> getProductsByName(String productName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.createQuery("from Product where productName like :product");
		List<Product> productList = query.setParameter("product", '%'+ productName+'%').getResultList();
	
		session.getTransaction().commit();
		session.close();

		return productList;
	}

	public List<Cart> getCartById(int cartId) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("Cart.byId");
		query.setParameter("cartId", cartId);
		List<Cart> cartList = (List<Cart>) query.getResultList();

		session.getTransaction().commit();
		session.close();

		return cartList;
	}

	public void addProductsToCart(Cart cart, Product product) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		cart.getProducts().add(product);

		session.save(cart);

		session.getTransaction().commit();
		session.close();
	}

	public List<Account> getAccountsByUsername(String userName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("Account.byUserName");
		query.setParameter("userName", userName);
		List<Account> accountList = (List<Account>) query.getResultList();

		session.getTransaction().commit();
		session.close();

		return accountList;
	}

	public List<BuyerAccount> getBuyersByUsername(String userName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<BuyerAccount> criteria = criteriaBuilder.createQuery(BuyerAccount.class);
		Root<BuyerAccount> root = criteria.from(BuyerAccount.class);
		criteria.where(criteriaBuilder.equal(root.get("userName"), userName));
		List<BuyerAccount> BuyerList = session.createQuery(criteria).getResultList();

		session.getTransaction().commit();
		session.close();

		return BuyerList;
	}

	public List<BuyerAccount> getBuyerCartByUsername(String userName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("BuyerAccount.byUserName");
		query.setParameter("userName", userName);
		List<BuyerAccount> buyerList = (List<BuyerAccount>) query.getResultList();

		session.getTransaction().commit();
		session.close();

		return buyerList;
	}

	public void connectBuyerToCart(String userName, Cart cart) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		BuyerAccount buyer = (BuyerAccount) session.get(BuyerAccount.class, userName);
		buyer.setCart(cart);

		session.update(buyer);
		saveCart(cart);

		session.getTransaction().commit();
		session.close();
	}

	public void connectSellerToProducts(String userName, Product product) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		SellerAccount seller = (SellerAccount) session.get(SellerAccount.class, userName);
		product.setSeller(seller);
		seller.getProducts().add(product);

		session.update(seller);
		session.save(product);

		session.getTransaction().commit();
		session.close();
	}

	public void connectProductsToCart(int productId, Cart cart) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Product product = (Product) session.get(Product.class, productId);
		product.getCarts().add(cart);
		cart.getProducts().add(product);

		session.save(product);
		session.save(cart);

		session.getTransaction().commit();
		session.close();
	}

	public List<Product> getConnectionsCartSeller(Product product) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("Seller.byConnection");
		query.setParameter("product", product);

		List<Product> products = query.getResultList();

		session.getTransaction().commit();
		session.close();

		return products;
	}

	public List<Product> getConnectionsCartProduct(Product product) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query query = session.getNamedQuery("Cart.byConnection");
		query.setParameter("product", product);

		List<Product> products = query.getResultList();

		session.getTransaction().commit();
		session.close();

		return products;
	}

	public void editingContact(String userName, BuyerAccount account) {

		Session session = sessionFactory.openSession();
		session.beginTransaction();

		BuyerAccount bAccount = (BuyerAccount) session.get(BuyerAccount.class, userName);
		bAccount.setFirstName(account.getFirstName());
		bAccount.setLastName(account.getLastName());
		bAccount.setEmail(account.getEmail());
		bAccount.getAddress().setCity(account.getAddress().getCity());
		bAccount.getAddress().setPostalCode(account.getAddress().getPostalCode());
		bAccount.getAddress().setStreet(account.getAddress().getStreet());
		session.save(bAccount);

		session.getTransaction().commit();
		session.close();
	}

	public void seller_addProduct(Product product, ProductOptions option, String seller) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		SellerAccount sellerAccount = (SellerAccount) session.get(SellerAccount.class, seller);
		product.setSeller(sellerAccount);
		sellerAccount.getProducts().add(product);

		session.getTransaction().commit();
		session.close();
	}
	

	public void upgradingToSeller(String userName,String compName, String description,String category,
			String email, String address, String phoneNumber, String prodName) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
//		List<Address> addressList= new ArrayList<Address>();
//		addressList.add(address);
		BuyerAccount buyer = (BuyerAccount) session.get(BuyerAccount.class, userName);
		SellerAccount s = new SellerAccount();
		s.setUserName(buyer.getUserName());
		s.setCompanyName(compName);
		s.setDateCreated(buyer.getDateCreated());
		s.setEmail(email);
		s.setFirstName(buyer.getFirstName());
		s.setLastName(buyer.getLastName());
		s.setPassword(buyer.getPassword());
		s.setRating(1);
		s.setStatus("Active");
		buyer.getAddresses().add(new Address(address, address, address));
		s.setAddresses(buyer.getAddresses());
		List<Product> productList= new ArrayList<Product>();
		Product p = new Product(prodName, category, new ProductOptions(description,1,1),s);
		productList.add(p);
		s.setProducts(productList);
		
		session.delete(buyer);
		session.save(s);
		
		
		
		
		
		session.getTransaction().commit();
		session.close();
	}
	
	
}
