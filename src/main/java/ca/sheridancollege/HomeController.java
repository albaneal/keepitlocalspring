/*
 * Dummy Account Credentials:
 * 
 * Admin Account:
 * Username: adminAccount 
 * Password: admin
 * 
 * Seller Account:
 * Username: sellerAccount
 * Password: seller
 * 
 * Buyer Account:
 * Username: buyerAccount
 * Password: buyer
 */
package ca.sheridancollege;

import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ca.sheridancollege.beans.Address;
import ca.sheridancollege.beans.BuyerAccount;
import ca.sheridancollege.beans.Cart;
import ca.sheridancollege.beans.Product;
import ca.sheridancollege.beans.ProductOptions;
import ca.sheridancollege.beans.SellerAccount;
import ca.sheridancollege.dao.DAO;

@Controller
public class HomeController {

	DAO dao = new DAO();

	@RequestMapping("/")
	public String showHome(Model model, Principal principal) {
		if (dao.getAccounts().isEmpty()) {
			dao.generateItems();
		}
		model.addAttribute("productList", dao.getProducts());
		return "th_index";
	}

	@RequestMapping("/login")
	public String loginToStore(Model model, Principal principal) {
		model.addAttribute("productList", dao.getProducts());
		return "th_login_signup";
	}

	@RequestMapping("/goToLogin")
	public String goToLogin() {
		return "th_login_signup";
	}

	@RequestMapping("/goToStore")
	public String goToStore(Model model) {
		model.addAttribute("productList", dao.getProducts());
		return "th_index";
	}

	@RequestMapping("/account/index")
	public String securedHome(Principal principal) {
		return "account/th_index";
	}

	@RequestMapping("/account/goToSecureStore")
	public String goToSecuredStore(Model model) {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().equals("[ROLE_ADMIN]")) {
			return "admin/th_index";
		} else {
			Boolean sellerOption = false;
			if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString()
					.equals("[ROLE_SELLER]")) {
				sellerOption = true;
			}
			model.addAttribute("sellerOption", sellerOption);
			model.addAttribute("productList", dao.getProducts());
			return "account/th_index";
		}
	}

	@RequestMapping("/account/checkMyOptions")
	public String checkMarketOptions(Model model) {
		Boolean buyerOption = false;
		Boolean sellerOption = false;
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().equals("[ROLE_ADMIN]")
				|| SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString()
						.equals("[ROLE_SELLER]")) {
			sellerOption = true;
		} else {
			buyerOption = true;
		}
		model.addAttribute("sellerOption", sellerOption);
		model.addAttribute("buyerOption", buyerOption);
		return "account/th_market_options";
	}

	@RequestMapping("/account/seller/addProduct")
	public String seller_addProductPage(Model model) {
		model.addAttribute("categoryList", new Product().getCategories());
		return "account/seller/th_seller_addProduct";
	}

	@RequestMapping("/account/seller/addProd")
	public String seller_addProductPageFinalize(Model model, @RequestParam String productName,
			@RequestParam String category, @RequestParam int productQTY, @RequestParam String description,
			Principal principal) {
		String seller = principal.getName();
		String output = "Product has been successfully added";
		ProductOptions option = new ProductOptions(description.trim(), productQTY, 0);
		Product product = new Product(productName.trim(), category, option, new SellerAccount());
		try {
			dao.seller_addProduct(product, option, seller);
		} catch (Exception ex) {
			output = "Someting went wrong, please try again later.";
		}
		model.addAttribute("notification", output);
		model.addAttribute("categoryList", new Product().getCategories());
		return "account/seller/th_seller_addProduct";
	}

	@RequestMapping("/account/getAccountOptions")
	public String getAccountOptions() {
		return "account/th_market_account_options";
	}

	@RequestMapping("/account/getChangeContact")
	public String getChangeContact(Model model, Principal principal) {
		String name = principal.getName();
		BuyerAccount account = dao.getBuyersByUsername(name).get(0);
		model.addAttribute("account", account);
		return "account/th_market_contact_options";
	}

	@GetMapping("/account/saveContact")
	public String goToMarketAccountContactOptions(Model model, Principal principal, @RequestParam String firstName,
			@RequestParam String lastName, @RequestParam String street, @RequestParam String email,
			@RequestParam String city, @RequestParam String postalCode) {
		String name = principal.getName();

		BuyerAccount account = dao.getBuyersByUsername(name).get(0);
		account.setFirstName(firstName);
		account.setLastName(lastName);
		account.setEmail(email);
		account.getAddress().setStreet(street);
		account.getAddress().setCity(city);
		account.getAddress().setPostalCode(postalCode);
		dao.editingContact(name, account);

		model.addAttribute("account", account);
		return "account/th_market_contact_options";
	}

	@RequestMapping("/account/addToCart/{id}")
	public String addToCart(Model model, @PathVariable int id, Principal principal) {
		String name = principal.getName();

		List<Product> productList = new ArrayList<Product>();
		productList.add(dao.getProductsById(id).get(0));

		if (dao.getBuyerCartByUsername(name).isEmpty() || dao.getBuyerCartByUsername(name) == null) {
			Cart cart = new Cart(true, 20, productList);
			dao.saveCart(cart);
			dao.connectBuyerToCart(name, cart);
		} else {
			Cart cart = dao.getBuyersByUsername(name).get(0).getCart();
			dao.connectProductsToCart(id, cart);
			dao.connectBuyerToCart(name, cart);
		}

		model.addAttribute("productList", dao.getProducts());
		return "account/th_index";
	}

	@RequestMapping("/account/clearCart")
	public String clearCart(Model model, Principal principal) {
		String name = principal.getName();
		BuyerAccount buyer = dao.getBuyersByUsername(name).get(0);

		buyer.getCart().getProducts().clear();
		dao.saveCart(buyer.getCart());

		if (!(dao.getBuyerCartByUsername(name).isEmpty()) && !(dao.getBuyerCartByUsername(name) == null)) {
			model.addAttribute("cartList", buyer.getCart().getProducts());
		}
		return "account/th_cart";
	}

	@GetMapping("/signUp")
	public String goToSignUp(Model model) {
		model.addAttribute("account", new BuyerAccount());
		return "th_signup";
	}

	@PostMapping("/signingUp")
	public String finalizeRegistration(Model model, @RequestParam(required = false) String username,
			@RequestParam(required = false) String password, @RequestParam(required = false) String email,
			@RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName,
			@RequestParam(required = false) String city, @RequestParam(required = false) String street,
			@RequestParam(required = false) String postalCode) {

		Address address = new Address(city, street, postalCode);

		BuyerAccount buyer = new BuyerAccount(username, email, password, LocalDate.now(), /* "buyer", */
				address, firstName, lastName, "active");
		String notification = username + " has been registered!";
		try {
			dao.saveBuyer(buyer);
		} catch (Exception ex) {
			notification = "Something went wrong, please try again later";
		}
		model.addAttribute("notification", notification);
		return "th_signup";
	}

	@RequestMapping("/searchProducts")
	public String searchProductsSignedOut(Model model, @RequestParam String search) {
		model.addAttribute("productList", dao.getProductsByName(search));
		return "th_index";
	}

	@RequestMapping("/searchProductsSecure")
	public String searchProductsSignedIn(Model model, @RequestParam String search) {
		model.addAttribute("productList", dao.getProductsByName(search));
		return "account/th_index";
	}

	@RequestMapping("/account/checkMyCart")
	public String checkCart(Model model, Principal principal) {
		String name = principal.getName();
		BuyerAccount buyer = dao.getBuyersByUsername(name).get(0);
		if (!(dao.getBuyerCartByUsername(name).isEmpty()) && !(dao.getBuyerCartByUsername(name) == null)) {
			model.addAttribute("cartList", buyer.getCart().getProducts());
		}
		return "account/th_cart";
	}

	@RequestMapping("/admin/searchUser")
	public String admin_searchUser(Model model) {
		model.addAttribute("accountList", dao.getAccounts());
		return "admin/th_searchUser";
	}

	@RequestMapping("/admin/search")
	public String admin_searching(Model model, @RequestParam String search) {
		model.addAttribute("accountList", dao.getAccountsByQueryUsername(search.trim()));
		return "admin/th_searchUser";
	}

	@RequestMapping("/admin/delete/{id}")
	public String admin_deleteUser(Model model, @PathVariable String id) {
		dao.adminDeleteUser(id);
		model.addAttribute("accountList", dao.getAccounts());
		return "admin/th_searchUser";
	}
	
	@RequestMapping("account/confirmUpdate")
	public String confirmingUpgrade(Model model, Principal principal, @RequestParam String name, @RequestParam String address, 
			@RequestParam String phoneNumber,@RequestParam String email, @RequestParam String productName, 
			@RequestParam String productDescription,@RequestParam String productCategory) {
		String userName = principal.getName();
		
		dao.upgradingToSeller(userName, name, productDescription, productCategory, email, address, phoneNumber, productName);
		
		return "account/th_market_accountOptions";
		
		
	}
	
	@GetMapping("/account/goToUpgradeToSeller")
	public String goToUpgarde() {
		
		return "account/th_upgradeToSeller";
	}

	@GetMapping("/access-denied")
	public String showError() {
		return "error/th_access-denied";
	}
}
