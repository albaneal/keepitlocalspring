package ca.sheridancollege.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "Cart.byConnection", query = "FROM Cart WHERE :product MEMBER OF products")
@NamedQuery(name = "Cart.byId", query = "FROM Cart WHERE cartId=:cartId")
public class Cart implements Serializable {

	@Id
	@GeneratedValue
	private int cartId;
	private boolean isRecurring;
	private double totalPrice;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.REMOVE })
	private List<Product> products = new ArrayList<Product>();

	public Cart(boolean isRecurring, double totalPrice, List<Product> products) {
		this.isRecurring = isRecurring;
		this.totalPrice = totalPrice;
		this.products = products;
	}
}
