package ca.sheridancollege.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Entity
public class Product implements Serializable {

	@Id
	@GeneratedValue
	private int productId;
	private String productName;
	private String category;

	@Transient
	private String[] categories;

	@Embedded
	private ProductOptions productOptions;

	@ManyToMany(cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.REMOVE }, mappedBy = "products")
	private List<Cart> carts = new ArrayList<Cart>();

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.REMOVE })
	private SellerAccount seller;

	public Product(String productName, String category, ProductOptions productOptions, SellerAccount seller) {
		this.productName = productName;
		this.category = category;
		this.productOptions = productOptions;
		this.seller = seller;
	}

	public Product() {
		this.categories = new String[] { "Fruits", "Vegetables", "Meat", "Fish", "Poultry" };
	}
}
