/*
 * Dummy Account Credentials:
 * 
 * Admin Account:
 * Username: adminAccount 
 * Password: admin
 * 
 * Seller Account:
 * Username: sellerAccount
 * Password: seller
 * 
 * Buyer Account:
 * Username: buyerAccount
 * Password: buyer
 */
package ca.sheridancollege.beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "Account.byUserName", query = "from Account where userName=:userName")
@Table(name = "accounts")
public class Account implements Serializable {
	@Id
	@Column(name = "userName", unique = true, nullable = false, length = 45)
	public String userName;
	public String email;
	public String password;
	public LocalDate dateCreated;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	@Cascade(CascadeType.DELETE)
	private Set<AccountRole> role = new HashSet<AccountRole>(0);

	@Embedded
	private Address address;

	public Account(String userName, String email, String password, LocalDate dateCreated, Address address) {
		this.userName = userName;
		this.email = email;
		this.password = password;
		this.dateCreated = dateCreated;
		this.address = address;
	}
}
