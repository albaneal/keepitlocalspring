/*
 * Dummy Admin Account Credentials:
 * Username: adminAccount 
 * Password: admin
 */

package ca.sheridancollege.beans;

import java.time.LocalDate;
import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AdminAccount extends Account {

	private String adminName;

	public AdminAccount(String userName, String email, String password, LocalDate dateCreated, // String role,
			Address address, String adminName) {
		super(userName, email, password, dateCreated, address);
		this.adminName = adminName;
	}
}
