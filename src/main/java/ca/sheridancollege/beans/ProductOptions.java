package ca.sheridancollege.beans;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class ProductOptions implements Serializable {

	private String description;
	private int quantityOnHand;
	private int productRating;
}
