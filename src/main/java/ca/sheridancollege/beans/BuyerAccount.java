/*
 * Dummy Buyer Account Credentials:
 * Username: buyerAccount 
 * Password: buyer
 */
package ca.sheridancollege.beans;

import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "BuyerAccount.byUserName", query = "select cart from BuyerAccount where userName=:userName")
public class BuyerAccount extends Account {

	private String firstName;
	private String lastName;
	private String status;

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH,
			CascadeType.REMOVE })
	private Cart cart;

	public BuyerAccount(String userName, String email, String password, LocalDate dateCreated, Address address,
			String firstName, String lastName, String status) {
		super(userName, email, password, dateCreated, address);
		this.firstName = firstName;
		this.lastName = lastName;
		this.status = status;
	}

}
