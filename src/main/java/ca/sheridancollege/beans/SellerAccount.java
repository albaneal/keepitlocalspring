/*
 * Dummy Seller Account Credentials:
 * Username: sellerAccount 
 * Password: seller
 */
package ca.sheridancollege.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@NamedQuery(name = "Seller.byConnection", query = "FROM SellerAccount WHERE :product MEMBER OF products")
public class SellerAccount extends BuyerAccount {

	private String companyName;
	private int rating;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "seller")
	private List<Product> products;

	public SellerAccount(String userName, String email, String password, LocalDate dateCreated,
			Address address, String firstName, String lastName, String status, String companyName, int rating,
			List<Product> products) {
		super(userName, email, password, dateCreated, address, firstName, lastName, status);
		this.companyName = companyName;
		this.rating = rating;
		this.products = products;
	}
}
