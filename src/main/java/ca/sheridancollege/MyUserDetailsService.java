package ca.sheridancollege;

import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ca.sheridancollege.beans.Account;
import ca.sheridancollege.beans.AccountRole;
import ca.sheridancollege.dao.DAO;

import java.util.*;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = new DAO().findUserByUserName(username);
		List<GrantedAuthority> authorities = buildUserAuthority(account.getRole());
		return buildUserForAuthentication(account, authorities);
	}

	private User buildUserForAuthentication(Account account, List<GrantedAuthority> authorities) {
		return new User(account.getUserName(), account.getPassword(), true, true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<AccountRole> userRoles) {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
		for (AccountRole role : userRoles) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		return new ArrayList<GrantedAuthority>(grantedAuthorities);
	}
}
