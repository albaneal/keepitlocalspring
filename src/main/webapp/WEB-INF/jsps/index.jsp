<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home Page</title>
</head>
<body>

	<h1> Welcome To Keep It Locak</h1>

	<c:url var="urlAdd" value="/addAccount"/>
	<form action="${urlAdd}" method="get">
		<input type="submit" value="Add Buyer Account"/>
	</form> 
	<br/>	

	<c:url var="urlView" value="/viewItems"/>
	<form action="${urlView}" method="get">
		<input type="submit" value="View Items"/>
	</form> 
	<br/>
	
	<c:url var="urlVerify" value="/verifyId"/>
	<form action="${urlVerify}" method="get">
		<input type="submit" value="Connect Items"/>
	</form>
	<br/>
	
	<c:url var="urlSearch" value="/searchItem"/>
	<form action="${urlSearch}" method="get">
		<input type="submit" value="Search Items"/>
	</form>
	<br/>
	
</body>
</html>